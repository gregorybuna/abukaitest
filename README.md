# README #

Here’s a quick project exercise to show off your capabilities. Please submit your project with a link to test the web page, source code, test cases (how did you structure your own testing?), and the database layout. Please make the page available on some sort of web site we can access for review (please employ a LAMP stack).


### What is this repository for? ###


This is for test/exam for abukai

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions
***************
get the source codes from
git@bitbucket.org:gregorybuna/abukaitest.git
clone it

then upload it to your server

***************
create a database and table
table name abukaiuser here is the structure below
------------------------------------------

CREATE TABLE IF NOT EXISTS `abukaiuser` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `picture` text NOT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

----------------------------------------------------
look for the database.php located on this path
abukaitest\application\config\database.php

then go to line 79 to 81 and change it according to your database login
-------------------------------
'hostname' => 'localhost',
	'username' => 'root',
	'password' => '',
	'database' => 'abukaitestgreg',
------------------------------------

after then you can test the link

https://[yourdomain]/abukaitest/

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

testing Customer Information Entry Form
1. go to https://gregbuna.respicare.ph/abukaitest/
then filled out the following fields
Lastname
Firstname
Email
City
country

There is a ‘Save’ button to save changes and a ‘Cancel’ button to reset the changes except for the image. Lastname, firstname, email and city should be free text fields. In the email field you have to make sure that the content really is an email address. If the text entered in the email field is not an email, there should of course be some sort of an alert informing the user that the email is incorrect. Country is a drop-down with ‘United States’, ‘Canada’, ‘Japan’, ‘United Kingdom’, ‘France’ and ‘Germany’. The image has an ‘Upload’ button for the user to upload an image (JPEG). When pressing the ‘Upload’ button, a file selector should show up, and the user can select a file from the local drive. The image gets uploaded. When reloading the page, all changes should be abandoned, except for the image upload. After submitting the data, the data should be stored in a MySQL table ‘customers that has varchar(255) for lastname, firstname, email, city, country, and a file path for the image. 

2. Customer Information Review Page based on email lookup
go to this link
you can change the email if you have registered an email before and use it
https://gregbuna.respicare.ph/abukaitest/pages/getuser?email=sss@sss.com
page should allow to pass an email as a get parameter of the URL (e.g. ?email=test@abukai.com), and then retrieve the information in the MYSQL ‘customers table for that user, including the lastname, firstname, city, country and the image that was previously uploaded.

3. 3. Mini Pocket Calculator as part of customer information review page
from this link
https://gregbuna.respicare.ph/abukaitest/pages/getuser?email=sss@sss.com
click on calculator button


pocket calculator in HTML and Javascript with number keys 0-9 and a ‘+’, ‘-‘ and ‘=’ button. The calculator only needs to add and subtract. The page should have result field and an iFrame. The calculator with the buttons and the display should be in separate iFrames. The idea is for the user to enter a basic math formula such as 54 +37 – 21 into the calculator with the keys. The display shows the keys entered, and when pressing ‘=’ the input field outside the iframe computes and gets the result (the summation or subtraction).  Given you are passing values around, you should also consider security aspects (as always).

you can see that all the details was retrieve base from the email use in the url parameter

4. Screen share utility on customer information page
go to this link
https://gregbuna.respicare.ph/abukaitest/pages/screenshare

put your name on the text box then click share screen button

after that you can copy and paste the link or send it to another person

Sometimes staff must urgently link in a co-worker so both people can see the information. Add a screen share tool to the page with Google Chrome: Using the Google Chrome API you can implement screen sharing with another user. Write a web page where the other user sees the content of your screen so he can e.g. share the customer information with somebody (e.g. a co-worker). When you give another user the URL, that user should see the whole screen to her. We will also use this functionality to review/test your code together to make a final decision.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact