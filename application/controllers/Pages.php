<?php
class Pages extends CI_Controller {

public function __construct()
        {
                parent::__construct();
                $this->load->model('user_model');
                $this->load->helper('url_helper');
				$this->load->helper('url');
        }
		
		   public function screenshare($page = 'screenshare')
		{
			$data['title'] = ucfirst($page); // Capitalize the first letter
			$this->load->view('pages/header', $data);
        $this->load->view('pages/'.$page, $data);
       $this->load->view('pages/footer', $data);
		}
	       public function calculator($page = 'calculator')
		{
			
			$data['title'] = ucfirst($page); // Capitalize the first letter
	# var_dump($data['record']);exit;
	 $this->load->view('pages/header', $data);
        $this->load->view('pages/'.$page, $data);
       $this->load->view('pages/footer', $data);
		}
		       public function getuser($page = 'getuser')
{
	if($this->input->get('email'))
	{
		$mlook = array('email' => $this->input->get('email'));
		$result = $this->user_model->get_user($mlook);
		$data['record'] = $result[0];
	}else{
		$data['error'] = "No Record Found / Email Parameter not used";
	}
	
	 $data['title'] = ucfirst($page); // Capitalize the first letter
	# var_dump($data['record']);exit;
	 $this->load->view('pages/header', $data);
        $this->load->view('pages/'.$page, $data);
       $this->load->view('pages/footer', $data);
}
        public function view($page = 'home')
{
        if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
                show_404();
        }

        $data['title'] = ucfirst($page); // Capitalize the first letter

  $this->load->view('pages/header', $data);
        $this->load->view('pages/'.$page, $data);
       $this->load->view('pages/footer', $data);
}

        public function registeruser()
{

        $data = $_POST;
		
		$target_dir = "uploads/";
$_FILES["picture"]["name"] = rand(5, 200).$_FILES["picture"]["name"];		
$target_file = $target_dir . basename($_FILES["picture"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image

    $check = getimagesize($_FILES["picture"]["tmp_name"]);
    if($check !== false) {
     #  echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
    #    echo "File is not an image.";
        $uploadOk = 0;
    }

// Check if file already exists
if (file_exists($target_file)) {
   # echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["picture"]["size"] > 12500000) {
   # echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
   # echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
  #  echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["picture"]["tmp_name"], $target_file)) {
     #   echo "The file ". basename( $_FILES["picture"]["name"]). " has been uploaded.";
    } else {
     #  echo "Sorry, there was an error uploading your file.";
    }
}
$data['picture'] = $_FILES["picture"]["name"];
		#var_dump($data);exit;
$this->user_model->register_user($data);
       $this->load->view('pages/header', $data);
        $this->load->view('pages/home', $data);
		$this->load->view('pages/footer', $data);
}

}