<!DOCTYPE html>
<html lang="en">
<head>
<title>Gregory Buna</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css" />
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="<?php echo base_url();?>css/uniform.css" />
<link rel="stylesheet" href="<?php echo base_url();?>css/select2.css" />
<link rel="stylesheet" href="<?php echo base_url();?>css/matrix-style.css" />
<link rel="stylesheet" href="<?php echo base_url();?>css/matrix-media.css" />
<link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
</head>
<body>



<!--sidebar-menu-->

<div id="sidebar"> <a href="#" class="visible-phone"><i class="icon icon-list"></i>Forms</a>
  <ul>
    <li><a href="index.html"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
    <li><a href="<?php echo base_url();?>"><i class="icon icon-signal"></i> <span> Entry Form</span></a> </li>
    <li><a href="<?php echo base_url();?>pages/getuser"><i class="icon icon-inbox"></i> <span>Review Page </span></a> </li>

    <li><a href="<?php echo base_url();?>pages/screenshare"><i class="icon icon-fullscreen"></i> <span>Screen share</span></a></li>
  
   
  </ul>
</div>