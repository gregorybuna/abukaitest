<style>


#background {
    width:300px;
    height:500px !important;
    background:gray;
    margin:50px auto;
  
}

button {
    border:0;
    color:#fff;
}

#result {
    display:block;
    font-family: sans-serif;
    width:230px;
    height:40px;
    margin:10px auto;
    text-align: right;
    border:0;
    background:#3b3535;
    color:#fff;
    padding-top:20px;
    font-size:20px;
    margin-left: 25px;
    outline: none;
    overflow: hidden;
    letter-spacing: 4px;
    position: relative;
    top:10px;
}

#result:hover {
    
    cursor: text;
    
}

#first-rows {
    margin-bottom: 20px;
    position: relative;
    top:10px;
}

.rows {
    width:300px;
    margin-top:10px;
}

#delete {
    width:110px;
    height:50px;
    margin-left:25px;
    border-radius:4px;
}

/* Aligning the division and dot button properly */
.fall-back {
    margin-left:3px !important;
}

/* Aligning the addition and equals to button properly */
.align {
    margin-left: 6px !important;
}

/* Button styling */
.btn-style {
    width:50px;
    height:50px;
    margin-left:5px;
    border-radius:4px;
}

.eqn {
    width:50px;
    height:50px;
    margin-left:5px;
    border-radius:4px;
}

.first-child {
 margin-left:25px;
}


/* Adding background color to the number values */
 .num-bg {
    background:#000;
    color:#fff;
    font-size:26px;
    cursor:pointer;
    outline:none;
    border-bottom:3px solid #333;
}

 .num-bg:active {
    background:#000;
    color:#fff;
    font-size:26px;
    cursor:pointer;
    outline:none;
    box-shadow: inset 5px 5px 5px #555;
}

/*Adding background color to the operator values */ 
.opera-bg {
    background:#333;
    color:#fff;
    font-size:26px;
    cursor: pointer;
    outline:none;
    border-bottom:3px solid #555;
}

.opera-bg:active {
    background:#333;
    color:#fff;
    font-size:26px;
    cursor: pointer;
    outline:none;
    box-shadow: inset 4px 4px 4px #555;
}

/*Adding a background color to the delete button */
.del-bg {
    background:#24b4de;
    color:#fff;
    font-size:26px;
    cursor: pointer;
    outline:none;
    border-bottom:3px solid #399cb9;
}

.del-bg:active {
    background:#24b4de;
    color:#fff;
    font-size:26px;
    cursor: pointer;
    outline:none;
    box-shadow: inset 4px 4px 4px #399cb9;
}

/*Adding a background color to the equals to button */
#eqn-bg {
    background:#17a928;
    color:#fff;
    font-size:26px;
    cursor: pointer;
    outline:none;
    border-bottom:3px solid #1f7a29;
}

#eqn-bg:active {
    background:#17a928;
    color:#fff;
    font-size:26px;
    cursor: pointer;
    outline:none;
    box-shadow: inset 4px 4px 4px #1f7a29;
}

@-webkit-keyframes bgChange {
    0% {
       background:#24b4de; 
    }
    
    50% {
      background:#17a928;
    }
    
    100% {
        background:#399cb9;
    }
}
.modal-body {
    max-height: 500px !important;
}
</style>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Form elements</a> <a href="#" class="current">Validation</a> </div>
    <h1>ABUKAI ENGINEERING PROJECT EXERCISE/TEST</h1>
  </div>   <div style="display: none;" id="result"></div>
  <?php if(isset($error)){ ?>
  <div class="alert alert-error alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
              <h4 class="alert-heading">Error!</h4>
              <?php echo $error; ?> </div>
  <?php } ?>			  
  <div class="container-fluid"><hr>
  <div class="row-fluid">
      <div class="span12">
	   <button class="btn btn-success btn-large" data-toggle="modal" data-target="#myModal"><div class="icon-table"></div>Calculator</button>
        <div class="widget-box">
		
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Customer Information Review Page based on email lookup</h5>
          </div>
		 
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="<?php echo base_url();?>pages/registeruser" enctype="multipart/form-data" name="basic_validate" id="basic_validate" novalidate="novalidate">
              <div class="control-group">
                <label class="control-label">Lastname</label>
                <div class="controls">
                  <input type="text" name="lastname" id="lastname" value="<?php if(isset($record['lastname'])){echo $record['lastname'];} ?>">
                </div>
              </div>
			   <div class="control-group">
                <label class="control-label">Firstname</label>
                <div class="controls">
                  <input type="text" name="firstname" id="firstname" value="<?php if(isset($record['firstname'])){echo $record['firstname'];} ?>">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Email</label>
                <div class="controls">
                  <input type="text" name="email" id="email" value="<?php if(isset($record['email'])){echo $record['email'];} ?>">
                </div>
              </div>
			  <div class="control-group">
                <label class="control-label">City</label>
                <div class="controls">
                  <input type="text" name="city" id="city" value="<?php if(isset($record['city'])){echo $record['city'];} ?>">
                </div>
              </div>
			   <div class="control-group">
              <label class="control-label">Country</label>
              <div class="controls">
                <input type="text" name="country" id="city" value="<?php if(isset($record['country'])){echo $record['country'];} ?>">
              </div>
            </div>
           <div class="control-group">
                <label class="control-label">Picture</label>
                <div class="controls">
              
<div ><img src="<?php echo base_url();?>/uploads/<?php if(isset($record['picture'])){echo $record['picture'];} ?>"></div>
                </div>
              </div>
             
          
            </form>
          </div>
        </div>
      </div>
    </div>
	</div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Mini Pocket Calculator</h4>
      </div>
      <div class="modal-body">
      <div id="background"><!-- Main background -->
	  start an iframe >>>
             <iframe  height="50px" id="frame-id" src="../result.html"></iframe>
    end an iframe <<<
             
         <div id="main">
             <div id="first-rows">
              <button class="del-bg" id="delete">Del</button>
               
                 <button value="+" class="btn-style opera-bg value align operator">+</button>
                 </div>
                 
               <div class="rows">
             <button value="7" class="btn-style num-bg num first-child">7</button>
                 <button value="8" class="btn-style num-bg num">8</button>
                 <button value="9" class="btn-style num-bg num">9</button>
                 <button value="-" class="btn-style opera-bg operator">-</button>
                 </div>
                 
                 <div class="rows">
                 <button value="4" class="btn-style num-bg num first-child">4</button>
                 <button value="5" class="btn-style num-bg num">5</button>
                 <button value="6" class="btn-style num-bg num">6</button>
              
                 </div>
                 
                 <div class="rows">
                 <button value="1" class="btn-style num-bg num first-child">1</button>
                 <button value="2" class="btn-style num-bg num">2</button>
                 <button value="3" class="btn-style num-bg num">3</button>
              
                 </div>
                 
                 <div class="rows">
                 <button value="0" class="num-bg zero" id="delete">0</button>
                 <button value="." class="btn-style num-bg period fall-back">.</button>
                 <button value="=" id="eqn-bg" class="eqn align">=</button>
                 </div>
                
             </div>
         
         </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script>

window.onload = function() {


var current,
    screen,
	screen2,
    output,
    limit,
    zero,
    period,
    operator;
    
    screen = document.getElementById("result");
 screen2 = document.getElementById("frame-id");
var elem = document.querySelectorAll(".num");
    
      var len = elem.length;
    
      for(var i = 0; i < len; i++ ) {
        
        elem[i].addEventListener("click",function() {
               //   alert(this.value);
				
            num = this.value;
                     
            output = screen.innerHTML +=num;
 screen2.src="javascript:'"+output+"'";
                  
            limit = output.length;
         
         if(limit > 16 ) {
        
         alert("Sorry no more input is allowed");
             
       }
       
     },false);
        
    } 

    document.querySelector(".zero").addEventListener("click",function() {
        
        zero = this.value;
        
        if(screen.innerHTML === "") {
            
           output = screen.innerHTML = zero;  
        }
        
        else if(screen.innerHTML === output) {
            
         output = screen.innerHTML +=zero;
            
        }
          
    },false);
    
    document.querySelector(".period").addEventListener("click",function() {
        
        period = this.value;
        
        if(screen.innerHTML === "") {
            
         output = screen.innerHTML = screen.innerHTML.concat("0.");
            
         }
    
        else if(screen.innerHTML === output) {
        
          screen.innerHTML = screen.innerHTML.concat(".");
		  screen2.src="javascript:'"+screen.innerHTML.concat(".")+"'";
            
        }
        
    },false);
    
    
    document.querySelector("#eqn-bg").addEventListener("click",function() {
        
      if(screen.innerHTML === output) {
          
        screen.innerHTML = eval(output);
		screen2.src="javascript:'"+eval(output)+"'";
      }
        
      else {
            screen.innerHTML = "";
			screen2.src="javascript:''";
      }
          
    },false);
    
 document.querySelector("#delete").addEventListener("click",function() {
     
        screen.innerHTML = "";
		 screen2.src="javascript:''";
        
    },false);
    
   
     var elem1 = document.querySelectorAll(".operator");
    
      var len1 = elem1.length;
    
      for(var i = 0; i < len1; i++ ) {
        
        elem1[i].addEventListener("click",function() {
  
        operator = this.value;
         
         if(screen.innerHTML === "") {
            
            screen.innerHTML = screen.innerHTML.concat("");
			screen2.src="javascript:'"+screen.innerHTML.concat("")+"'";
            
        }
        
        else if(output) {
        
            screen.innerHTML = output.concat(operator);
			screen2.src="javascript:'"+output.concat(operator)+"'";
        }
           
    },false);
          
      }   
}
</script>