<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Form elements</a> <a href="#" class="current">Validation</a> </div>
    <h1>ABUKAI ENGINEERING PROJECT EXERCISE/TEST</h1>
  </div>
  <div class="container-fluid"><hr>
  <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Customer Information Entry Form</h5>
          </div>
          <div class="widget-content nopadding">
            <form class="form-horizontal" method="post" action="<?php echo base_url();?>pages/registeruser" enctype="multipart/form-data" name="basic_validate" id="basic_validate" novalidate="novalidate">
              <div class="control-group">
                <label class="control-label">Lastname</label>
                <div class="controls">
                  <input required="true" type="text" name="lastname" id="lastname">
                </div>
              </div>
			   <div class="control-group">
                <label class="control-label">Firstname</label>
                <div class="controls">
                  <input required="true" type="text" name="firstname" id="firstname">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Email</label>
                <div class="controls">
                  <input type="text" name="email" id="email">
                </div>
              </div>
			  <div class="control-group">
                <label class="control-label">City</label>
                <div class="controls">
                  <input required="true" type="text" name="city" id="city">
                </div>
              </div>
			   <div class="control-group">
              <label class="control-label">Country</label>
              <div class="controls">
                <select name="country" id="country">
                  <option value="United States">United States</option>
                  <option value="Canada">Canada</option>
                  <option value="Japan">Japan</option>
                  <option value="United Kingdom">United Kingdom</option>
                  <option value="France">France</option>
                  <option value="Germany">Germany</option>
                  
                </select>
              </div>
            </div>
           <div class="control-group">
                <label class="control-label">Picture</label>
                <div class="controls">
                 <input required="true" class="classhere" type="file" name="picture" id="picture" />
<div class="imagearea"></div>
                </div>
              </div>
             
              <div class="form-actions">
                <input type="submit" value="Submit" class="btn btn-success">
				<input type="reset" value="Reset" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
	</div>
</div>
